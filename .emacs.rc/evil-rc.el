(rc/require 'evil)
(evil-mode 1)

;;; Configuration
(setq evil-emacs-state-modes nil)

;;; Keybindings
;; Free up keybindings
(eval-after-load "evil-maps"
  (define-key evil-motion-state-map (kbd "SPC") nil))

;;; Evil Mode Keybindings
(define-key evil-normal-state-map (kbd "SPC TAB") 'mode-line-other-buffer)
