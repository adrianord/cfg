;;; Configuration
(ido-mode 1)
(setq backup-directory-alist '(("." . "~/.emacs_saves")))
(setq x-select-enable-clipboard t)

;;; Keybindings
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
