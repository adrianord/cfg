(rc/require 'helm)

;;; Configuration
(setq helm-echo-input-in-header-line t)
(setq helm-mode-fuzzy-match t)
(setq helm-M-x-fuzzy-match t)
(setq helm-always-two-windows nil)
(setq helm-display-buffer-default-height 23)
(setq helm-default-display-buffer-functions '(display-buffer-in-side-window))

;;; Keybindings
(define-key evil-normal-state-map (kbd "SPC f f") 'helm-find-files)
(define-key evil-normal-state-map (kbd "SPC b b") 'helm-mini)
(define-key evil-normal-state-map (kbd "SPC SPC") #'helm-M-x)

;;; Hooks
(add-hook 'helm-minibuffer-set-up-hook 'helm-hide-minibuffer-maybe)

;;; Functions
(defun helm-hide-minibuffer-maybe ()
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face (let ((bg-color (face-background 'default nil)))
                              `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))

