(rc/require 'lsp-mode)
(rc/require 'lsp-ui)

(add-hook 'lsp-mode-hook 'lsp-ui-mode)
