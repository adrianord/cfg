;; Gui
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(show-paren-mode 1)

;; Disable default splash screen
(setq inhibit-startup-message t)

;; Theme
(rc/require-theme 'monokai)
