(rc/require 'intero)
(rc/require 'lsp-haskell)

(intero-global-mode 1)
(add-hook 'haskell-mode 'intero-mode)
(add-hook 'haskell-mode-hook #'lsp-haskell-enable)
(add-hook 'haskell-mode-hook #'flycheck--mode)

