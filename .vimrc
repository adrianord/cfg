""""""""""""""""""""""""""""""""""""""""""
" Adrian Ordonez Vimrc configuration
""""""""""""""""""""""""""""""""""""""""""
runtime user/defaults.vim
runtime user/vundle.vim

""""""""""""""""""""""""""""""""""""""""""
" General Configuration Section
""""""""""""""""""""""""""""""""""""""""""
" Show linenumbers
set number
set relativenumber
set ruler

" Always display the status line
set laststatus=2

" Set Proper Tabs
set tabstop=4
set shiftwidth=4
set expandtab

" Set Backspace Behavior
set backspace=2

" Set list settings
set listchars=tab:>-,trail:~,extends:>,precedes:<
" set list

" Highlight line
set cursorline

" Theme and Styling
set t_Co=256
set background=dark

if (has("termguicolors"))
  set termguicolors
endif

let base16colorspace=256  " Access colors present in 256 colorspace
colorscheme spacemacs-theme

let g:spacegray_underline_search = 1
let g:spacegray_italicize_comments = 1

" Clean up gVim interface
:set guioptions-=m  "remove menu bar
:set guioptions-=T  "remove toolbar
:set guioptions-=r  "remove right-hand scroll bar
:set guioptions-=L  "remove left-hand scroll bar

""""""""""""""""""""""""""""""""""""""""""
" Plugin Configuration Section
""""""""""""""""""""""""""""""""""""""""""
" OmniSharp
let g:OmniSharp_server_path = '/c/OmniSharp/OmniSharp.exe'
let g:syntastic_cs_checkers = ['code_checker']
" let g:OmniSharp_selector_ui = 'ctrlp'
let g:OmniSharp_prefer_global_sln = 1
let g:OmniSharp_timeout = 10

" Vim-Airline Configuration
set guifont=SpaceMono_NF:h9
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='base16'
let g:airline_left_sep = "\uE0B8"
let g:airline_right_sep = "\uE0BA"
let g:hybrid_custom_term_colors = 1
let g:hybrid_reduced_contrast = 1

" indentLine Configuration
let g:indentLine_leadingSpaceChar = '·'
let g:indentLine_leadingSpaceEnabled = 1
"
""""""""""""""""""""""""""""""""""""""""""
" Plugin Keymapping Section
""""""""""""""""""""""""""""""""""""""""""
nnoremap <leader>ft :NERDTreeToggle<CR>
nnoremap <leader>tt :TagbarToggle<CR>

""""""""""""""""""""""""""""""""""""""""""
" User Keymapping Section
""""""""""""""""""""""""""""""""""""""""""

" File Commands
nnoremap <leader>fs :w<CR>
nnoremap <leader>fS :wa<CR>

" Editor
nnoremap <leader><tab> :b#<CR>
nnoremap <leader>qq :qa<CR>
nnoremap <leader>qQ :qa!<CR>

" Split
nnoremap <leader>wv :vsplit<CR>
nnoremap <leader>ws :split<CR>
nnoremap <leader>wh <c-w>h
nnoremap <leader>wj <c-w>j
nnoremap <leader>wk <c-w>k
nnoremap <leader>wl <c-w>l
nnoremap <leader>wJ <c-w>J
nnoremap <leader>wd :q<CR>
nnoremap <leader>wD :on<CR>

" Search visually selected text
vnoremap // y/<C-R>"<CR>

" Delete previous word with Ctrl+Backspace
imap  <C-W>
imap <C-BS> <C-W>

" Implement Maximize without removing other windows
nnoremap <C-W>O :call MaximizeToggle()<CR>
nnoremap <C-W>o :call MaximizeToggle()<CR>
nnoremap <C-W><C-O> :call MaximizeToggle()<CR>

"-------
" Cursor
let &t_ti.="\e[2 q"
let &t_SI.="\e[6 q"
let &t_EI.="\e[2 q"
let &t_te.="\e[0 q"
""""""""""""""""""""""""""""""""""""""""""
" Functions Section
""""""""""""""""""""""""""""""""""""""""""

" Maximize window without removing other windows
function! MaximizeToggle()
  if exists("s:maximize_session")
    exec "source " . s:maximize_session
    call delete(s:maximize_session)
    unlet s:maximize_session
    let &hidden=s:maximize_hidden_save
    unlet s:maximize_hidden_save
  else
    let s:maximize_hidden_save = &hidden
    let s:maximize_session = tempname()
    set hidden
    exec "mksession! " . s:maximize_session
    only
  endif
endfunction

" Sign Dummy
autocmd BufEnter * sign define dummy
autocmd BufEnter * execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')
