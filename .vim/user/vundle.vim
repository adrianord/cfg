"""" Vundle Configuration BEGIN
" disable file type for vundle
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
Plugin 'VundleVim/Vundle.vim'

""""""""""""""""""""""""""""""""""""""""""
" Plugins Section
""""""""""""""""""""""""""""""""""""""""""

" Utility
Plugin 'scrooloose/nerdtree'
Plugin 'majutsushi/tagbar'
Plugin 'BufOnly.vim'
Plugin 'wesQ3/vim-windowswap'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'Shougo/neocomplete.vim'
Plugin 'jeetsukumaran/vim-buffergator'
Plugin 'gilsondev/searchtasks.vim'
Plugin 'tpope/vim-dispatch'
Plugin 'tpope/vim-fugitive'
Plugin 'terryma/vim-multiple-cursors'

" Programming Support
Plugin 'vim-syntastic/syntastic'
Plugin 'leafgarland/typescript-vim'
Plugin 'prettier/vim-prettier', { 'do': 'yarn install' }
if has('python')
    Plugin 'davidhalter/jedi-vim'
    Plugin 'OmniSharp/omnisharp-vim'
endif

" Theme / Interface
Plugin 'AnsiEsc.vim'
Plugin 'ryanoasis/vim-devicons'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'colepeters/spacemacs-theme.vim'
Plugin 'Yggdroot/indentLine'

call vundle#end()
filetype plugin indent on

"""" Vundle Configuration END
