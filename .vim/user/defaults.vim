" Set leader key to <Space>
let mapleader=" "
"
" Set system clipboard
set clipboard=unnamed
"
" Disable Default
nnoremap <leader> <Nop>
"
" Get rid of delay between switching from Insert to Normal
set ttimeoutlen=0
set nocompatible
set nowrap
set encoding=utf-8
syntax on

" Allow hiding buffers
set hidden

" Ignore and Smart case
set ignorecase
set smartcase

" Lazy redraw
set lazyredraw

" Set default split to bottom
set splitbelow
