;;; RCs

(package-initialize)

(load "~/.emacs.rc/rc.el")

(load "~/.emacs.rc/evil-rc.el") ;; Needs to be loaded first for keybindings
(load "~/.emacs.rc/helm-rc.el")
(load "~/.emacs.rc/lsp-mode-rc.el")
(load "~/.emacs.rc/which-key-rc.el")
(load "~/.emacs.rc/company-rc.el")
(load "~/.emacs.rc/intero-rc.el")
(load "~/.emacs.rc/common-rc.el")
(load "~/.emacs.rc/appearance-rc.el")
(load "~/.emacs.rc/eshell-rc.el")
(load "~/.emacs.rc/org-mode-rc.el")

;;; Set Custom File
(setq custom-file "~/.emacs-custom.el")
(load custom-file)
