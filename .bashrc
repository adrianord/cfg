#TERM=xterm-256color
env=~/.ssh/agent.env
# bash -c zsh
bind '"":""'

agent_load_env () { test -f "$env" && . "$env" >| /dev/null ; }

agent_start () {
    (umask 077; ssh-agent >| "$env")
    . "$env" >| /dev/null ; }

agent_load_env

# agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)

if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
    agent_start
    ssh-add
elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
    ssh-add
fi

unset env

if [ -f /usr/share/bash-completion/completions/git ]; then
    source /usr/share/bash-completion/completions/git
fi

alias g='git'
alias c='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
__git_complete g __git_main
__git_complete c __git_main

alias dotnet='cmd //C "C:\Program Files\dotnet\dotnet.exe"'
alias e='exit'
alias q='exit'
alias v='vim'

mcd () {
    mkdir -p $1
    cd $1
}

rmvs () {
    while [ -d .vs ]; do 
        rm -r .vs
        sleep 1
    done
}

