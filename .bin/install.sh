git clone --bare git@bitbucket.org:adrianord/cfg.git $HOME/.cfg
function config {
    git --git-dir=$HOME/.cfg/ --work-tree=$HOME/.cfg/ --work-tree=$HOME $@
}
mkdir -p .config-backup
config checkout
if [ $? = 0] then
    echo "Check out config.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
fi;
config checkout
config config status.showUntrackedFiles no
